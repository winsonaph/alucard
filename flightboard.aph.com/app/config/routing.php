<?php
/**
 * Created by PhpStorm.
 * User: Waldek
 * Date: 12/01/2015
 * Time: 09:38
 */

// Create the router
$router = new \Phalcon\Mvc\Router();

//Remove trailing slashes automatically
$router->removeExtraSlashes(true);

//Define error page
$router->add(
    "/error",
    array(
        "controller" => "index",
        "action"     => "error",
    )
);

//Define Flight screens for specific car park route
$router->add(
    "/([a-zA-z]*)",
    array(
        "controller" => "index",
        "action"     => "index",
        "carpark_label" => 1,
    )
);

$router->handle();
