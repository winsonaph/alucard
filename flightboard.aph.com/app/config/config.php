<?php

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'portal_aph',
        'password'    => 'Rt97KOwf{zbaMrT12P3n7[FkqXpRzM',
        'dbname'      => 'portal_aph_com',
    ),
    'dev_database' => array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'aph_portal_dev',
        'password'    => 'Qy9EKIm0si9CsRVII9S2',
        'dbname'      => 'aph_portal_dev',
    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'pluginsDir'     => __DIR__ . '/../../app/plugins/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'adaptersDir'    => __DIR__ . '/../../app/adapters/',
        'helpersDir'     => __DIR__ . '/../../app/helpers/',
        'cacheDir'       => __DIR__ . '/../../app/cache/',
        'imagesDir'      => __DIR__ . '/../../public/assets/uploads/',
        'baseUri'        => '/',
        'cryptSalt'      => ')5P<y5{[GJ<yXKqeOVp{Lm}[s>BGuv9d'
    ),
    'mailer' => array(
        'smtpauth'      => false,
        'host'          => '192.168.201.72',
        'username'      => '',
        'password'      => '',
        'port'          => 25,
        'from'          => 'application@aph.com',
        'from_name'     => 'APH Flightboard',
        'admin_email'   => 'waldek@aph.com'
    ),
     /** Configuration for upload path for HelperImageUpload**/
    'image_upload_directory_list' => array(
    'flightscreens' => array(
        'original_directory_config' => "flightscreens/original/",
        'image_config_list' => array(
            "preview"=>array("directory"=>"flightscreens/preview/", "width"=>500, "height"=>345),
            "full"=>array("directory"=>"flightscreens/full/", "width"=>1080, "height"=>700),
            "window"=>array("directory"=>"flightscreens/window/", "width"=>800, "height"=>550),
        )
    ),
)
));
