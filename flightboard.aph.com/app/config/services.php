<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Cache\Backend\Libmemcached;

require_once $config->application->adaptersDir.'Smarty.php';

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, true);

$di->set('config', function () use ($config) {
    return $config;
});

/**
 * Setup router
 */

$di->set('router', function(){
    $router = null;
    require "routing.php";
    return $router;
});


//Setting up the view component (Smarty Based)
$di->set('view', function() {

    $view = new \Phalcon\Mvc\View();

    $view->setViewsDir('../app/views/');

    $view->registerEngines(
        array(".tpl" => function($view, $di){
            $smarty = new \Phalcon\Mvc\View\Engine\Smarty($view, $di);

            $smarty->setOptions(array(
                'template_dir'      => $view->getViewsDir(),
                'compile_dir'       => '../app/cache/templates_c',
                'error_reporting'   => error_reporting() ^ E_NOTICE,
                'escape_html'       => false,
                '_file_perms'       => 0666,
                '_dir_perms'        => 0777,
                'force_compile'     => false,
                'compile_check'     => true,
                'caching'           => false,
                'debugging'         => true,
            ));

            return $smarty;

        }
        )
    );
    return $view;
});

/**
 * Add Smarty to the dependency injections
 * Sometimes we need access to native Smarty features like 'fetch' to pull content for emails.
 */
$di->set('smarty',function() use ($config){
    $smarty = new Smarty();

    $smarty->template_dir = $config->application->viewsDir;
    $smarty->compile_dir  = $config->application->cacheDir.'/templates_c';
    $smarty->error_reporting = error_reporting() ^ E_NOTICE;
    //$smarty->debugging       = true;
    $smarty->escape_html     = false;

    return $smarty;
});


/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {
    return new DbAdapter(array(
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname
    ));
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('megabeast_db', function () use ($config, $di) {
    $connection = new DbAdapter(array(
        'host' => $config->megabeast_database->host,
        'username' => $config->megabeast_database->username,
        'password' => $config->megabeast_database->password,
        'dbname' => $config->megabeast_database->dbname
    ));
    /**
     * Attache events manager
     */
    $connection->setEventsManager($di->get('eventsManager'));

    return $connection;
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();
    return $session;
});

/**
 * Crypt service
 */
$di->set('crypt', function () use ($config) {
    $crypt = new Crypt();
    $crypt->setKey($config->application->cryptSalt);
    return $crypt;
});

$di->set('security', function(){

    /**
     * The default Phalcon uses the $2a$ salt prefix which has proven to have a weakness. This code will set the default prefix to $2y$ which is currently the recommended one. It also increases the work factor from 08 to 13.
     */
    $security = new Phalcon\Security();
    $security->setWorkFactor(13);
    $security->setDefaultHash(Phalcon\Security::CRYPT_BLOWFISH_Y);
    return $security;
}, true);

//Set up the flash service
$di->set('flash', function() {
    $flash = new Phalcon\Flash\Direct([
        'error' => 'alert alert-danger',
        'success' => 'alert alert-success',
    ]);
    $flash->setImplicitFlush(false);
    $flash->setAutomaticHtml(true);
    return $flash;
});


$di->set('cache', function() use($config) {
    // Cache the files for 1 day using a Data frontend
    $frontCache = new Phalcon\Cache\Frontend\Data(array(
        "lifetime" => 86400
    ));

// Create the component that will cache "Data" to a "File" backend
// Set the cache file directory - important to keep the "/" at the end of
// of the value for the folder
    /** file cache  */
//    $cache = new Phalcon\Cache\Backend\File($frontCache, array(
//        "cacheDir" => $config->application->cacheDir.'phalcon/'
//    ));


    /**
     * Memcache
     */
    $cache = new Phalcon\Cache\Backend\Libmemcached($frontCache, array(
        "servers" => array(
            array(
                "host" => "127.0.0.1",
                "port" => "11211",
                "weight" => "1"
            )
        )
    ));
    return $cache;
});

$di->set('dispatcher', function() {

    $eventsManager = new \Phalcon\Events\Manager();

    $eventsManager->attach("dispatch:beforeException", function($event, $dispatcher, $exception) {

        //Handle 404 exceptions
        if ($exception instanceof \Phalcon\Mvc\Dispatcher\Exception) {
            $dispatcher->forward(array(
                'controller' => 'index',
                'action' => 'error'
            ));
            return false;
        }

        //Handle other exceptions
        $dispatcher->forward(array(
            'controller' => 'index',
            'action' => 'error'
        ));
        return false;
    });

    $dispatcher = new \Phalcon\Mvc\Dispatcher();

    //Bind the EventsManager to the dispatcher
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;

}, true);


$di->set('mailer',function () use ($config){

    $mailer             = new PHPMailer();
    $mailer->isSMTP();
    //$mailer->isMail();
    $mailer->SMTPAuth   = $config->mailer->smtpauth;
    $mailer->Host       = $config->mailer->host;
    $mailer->Username   = $config->mailer->username;
    $mailer->Password   = $config->mailer->password;
    $mailer->From       = $config->mailer->from;
    $mailer->FromName   = $config->mailer->from_name;
    $mailer->Port       = $config->mailer->port;
    //$mailer->SMTPSecure = 'tls';

    //$mailer->SMTPDebug = true;
    $mailer->isHTML(true);
    $mailer->addBCC($config->mailer->admin_email);

    return $mailer;
});


