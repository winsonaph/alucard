<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->modelsDir,
    )
)->register();


$loader->registerNamespaces(
    array(
        'PortalDB' => $config->application->modelsDir,
    )
);

$loader->registerClasses(
    array(
        'HelperFormat' => $config->application->helpersDir.'Format.php',
        'HelperPaginate' => $config->application->helpersDir.'HelperPaginate.php',
    )
);

$loader->register();
