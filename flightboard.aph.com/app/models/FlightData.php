<?php

namespace PortalDB;

class FlightData extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $airport_id;

    /**
     *
     * @var string
     */
    public $arrival_departure;

    /**
     *
     * @var string
     */
    public $airline;

    /**
     *
     * @var string
     */
    public $flight_nos;

    /**
     *
     * @var string
     */
    public $scheduled_time;

    /**
     *
     * @var string
     */
    public $date;

    /**
     *
     * @var string
     */
    public $estimated_time;

    /**
     *
     * @var string
     */
    public $city1;

    /**
     *
     * @var string
     */
    public $city2;

    /**
     *
     * @var string
     */
    public $status;

    /**
     *
     * @var string
     */
    public $gate;

    /**
     *
     * @var string
     */
    public $bag;

    /**
     *
     * @var string
     */
    public $terminal;

    /**
     *
     * @var string
     */
    public $aircraft_type;

    /**
     *
     * @var string
     */
    public $last_updated;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('airport_id', 'PortalDB\Airports', 'id', array('alias' => 'Airports'));
    }

    public function getSource()
    {
        return 'flight_data';
    }

}
