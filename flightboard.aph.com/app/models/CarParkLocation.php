<?php

namespace PortalDB;

class CarParkLocation extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $label;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $airport_id;

    /**
     *
     * @var string
     */
    public $address1;

    /**
     *
     * @var string
     */
    public $address2;

    /**
     *
     * @var string
     */
    public $address3;

    /**
     *
     * @var string
     */
    public $town;

    /**
     *
     * @var string
     */
    public $post_code;

    /**
     *
     * @var id
     */
    public $county_id;

    /**
     *
     * @var string
     */
    public $country_id;

    /**
     *
     * @var integer
     */
    public $latitude;

    /**
     *
     * @var integer
     */
    public $longitude;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('county_id', 'PortalDB\County', 'id', array('alias' => 'County'));
        $this->belongsTo('country_id', 'PortalDB\Country', 'id', array('alias' => 'Country'));
        $this->belongsTo('airport_id', 'PortalDB\Airports', 'id', array('alias' => 'Airports'));
        $this->hasMany('id', 'PortalDB\FlightScreens', 'carpark_id', array('alias' => 'FlightScreens'));
    }

    public function getSource()
    {
        return 'car_park_location';
    }

}
