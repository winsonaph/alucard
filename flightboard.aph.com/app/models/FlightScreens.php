<?php

namespace PortalDB;

class FlightScreens extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $carpark_id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $intro;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $screen_image_name;

    /**
     *
     * @var integer
     */
    public $id_order;

    /**
     *
     * @var string
     */
    public $status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('carpark_id', 'PortalDB\CarParkLocation', 'id', array('alias' => 'CarParkLocation'));
    }

    public function getSource()
    {
        return 'flight_screens';
    }

}
