<?php

namespace PortalDB;

class Airports extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $code;

    /**
     *
     * @var string
     */
    public $address1;

    /**
     *
     * @var string
     */
    public $address2;

    /**
     *
     * @var string
     */
    public $address3;

    /**
     *
     * @var string
     */
    public $town;

    /**
     *
     * @var string
     */
    public $post_code;

    /**
     *
     * @var id
     */
    public $dice_fids_enabled;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('county_id', 'PortalDB\County', 'id', array('alias' => 'County'));
        $this->belongsTo('country_id', 'PortalDB\Country', 'id', array('alias' => 'Country'));
        $this->hasMany('id', 'PortalDB\FlightData', 'airport_id', array('alias' => 'FlightData'));
        $this->hasMany('id', 'PortalDB\CarParkLocation', 'airport_id', array('alias' => 'CarParkLocation'));
    }

    public function getSource()
    {
        return 'airports';
    }

}
