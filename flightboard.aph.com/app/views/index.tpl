{strip}<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <title>APH Portal - Flightboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN CSS TEMPLATE -->
    <link href="/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/flightboard.css" rel="stylesheet" type="text/css"/>
    <!-- END CSS TEMPLATE -->
</head>
<!-- BEGIN BODY -->
<body>
<!-- BEGIN PAGE CONTAINER-->
<div role="tabpanel">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist" id="myTab" style="display: none;">
        {counter assign=i start=1}
        {foreach $FLIGHT_DATA_LIST as $FlightTable}
            <li role="presentation" {if $i == 1}class="active"{/if}><a href="#flightboard{$i}" aria-controls="#flightboard{$i}" role="tab" data-toggle="tab">Flightboard {$i}</a></li>
            {counter}
        {/foreach}
        {foreach $FLIGHT_SCREENS_LIST as $FlightScreen}
            <li role="presentation" {if $i == 1}class="active"{/if}><a href="#flightscreen{$i}" aria-controls="#flightscreen{$i}" role="tab" data-toggle="tab">Flightscreen {$i}</a></li>
            {counter}
        {/foreach}
    </ul>
    <div class="tab-content">
        {counter assign=i start=1}
        {foreach $FLIGHT_DATA_LIST as $FlightTable}
            <div role="tabpanel" class="tab-pane {*fade*}{if $i == 1} {*in*} active{/if}" id="flightboard{$i}" style="background-color: #000">
                <h4><span class="semi-bold">{$FlightTable[0]->Airports->name} Airport</span> - Flight Departure Information
                </h4>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Flight No</th>
                        <th>City</th>
                        <th>Time</th>
                        <th>Est. Time</th>
                        <th>Status</th>
                        <th>Terminal</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $FlightTable as $FlightData}
                        <tr>
                            <td>{$FlightData->airline}-{$FlightData->flight_nos}</td>
                            <td>{$FlightData->city1} <i>{$FlightData->city2}</i></td>
                            <td>{$FlightData->scheduled_time|truncate:5:""}</td>
                            <td>{$FlightData->estimated_time|truncate:5:""}</td>
                            <td style="{if $FlightData->status == "DELAYED"}color:#FF0000{elseif $FlightData->status == "GATE OPEN"}color:#00ff00{/if}">{if $FlightData->status == NULL}TBA{else}{$FlightData->status}{/if}</td>
                            <td>{if $FlightData->terminal == ""}TBA{else}{$FlightData->terminal}{/if}</td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                <div class="row">
                    <div id="pagination_text" class="col-md-12">
                        <p>Page {$i} of {$MAXIMUM_FLIGHTBOARD}</p>
                    </div>
                </div>
            </div>
            {counter}
        {/foreach}
        {foreach $FLIGHT_SCREENS_LIST as $FlightScreen}
            <div role="tabpanel" class="tab-pane {*fade*}{if $i == 1} {*in*} active{/if}" id="flightscreen{$i}">
                {if $FlightScreen->screen_option == "full"}
                    <div class="row">
                        <div class="full col-md-12">
                            <img src="{$IMG_SOURCE}{$FlightScreen->screen_image_name}"/>
                        </div>
                    </div>
                {else}
                    <div class="grid simple">
                        <div class="grid-title">
                            <h2><span class="semi-bold">{$FlightScreen->CarParkLocation->name}</span> - {$FlightScreen->title}</h2>
                        </div>
                        <div class="grid-body" style="padding-top: 5px; min-height: 3000px;">
                            <h3>{$FlightScreen->description}</h3>
                            <br/>
                            {if !$FlightScreen->screen_image_name == NULL}
                            <div class="window col-md-offset-2 col-md-8">
                                <img src="{$IMG_SOURCE}{$FlightScreen->screen_image_name}"/>
                            </div>
                            {/if}
                        </div>
                    </div>
                {/if}
            </div>
            {counter}
        {/foreach}
    </div>
</div>
<!-- END PAGE -->
<!-- END CONTAINER -->
<script src="/assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="/assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/js/flightboard.js?version=1" type="text/javascript"></script>
</body>
</html>
{/strip}