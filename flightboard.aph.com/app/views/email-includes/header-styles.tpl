<style type="text/css">
    {literal}
    /* Mobile-specific Styles */
    @media only screen and (max-device-width: 480px) {
        img {
            height: auto;
            line-height: 100%;
        }
    }

    /* Client-specific Styles */
    body {
        width: 100% !important;
    }

    /* Reset Styles */
    body {
        background-color: #eeeeee;
        margin: 0;
        padding: 0;
    }

    img {
        outline: none;
        text-decoration: none;
        display: block;
        border: none;
    }

    br, strong br, b br, em br, i br {
        line-height: 100%;
    }

    h1, h2, h3, h4, h5, h6 {
        line-height: 100% !important;
        -webkit-font-smoothing: antialiased;
    }

    h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {
        color: #F47721 !important;
    }

    /* Orange */
    h1 a:active, h2 a:active, h3 a:active, h4 a:active, h5 a:active, h6 a:active {
        color: #F47721 !important;
    }

    h1 a:visited, h2 a:visited, h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
        color: #00121F !important;
    }

    table td, table tr {
        border-collapse: collapse;
    }

    code {
        white-space: normal;
        word-break: break-all;
    }

    #background-table {
        background-color: #eeeeee;
    }

    body, td {
        font-family: Georgia, "Times New Roman", Times, serif;
    }

    {/literal}
</style>