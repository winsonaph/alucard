<tr style="border-collapse: collapse;">
    <td align="left" valign="top" style="border-collapse: collapse;">
        <table width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
            <tr style="border-collapse: collapse;">
                <td width="640" height="14"><img border="0"
                                                 style="border:none; padding:0px; margin:0px; display:block; vertical-align:top;"
                                                 src="http://aph.com/assets/images/email/footer_scoreline.gif"
                                                 width="640" height="14"/></td>
            </tr>
            <tr style="border-collapse: collapse;">
                <td width="640" height="6"></td>
            </tr>
            <tr>
                <td width="640" valign="top">
                    <table width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
                        <tbody>
                        <tr style="border-collapse: collapse;">
                            <td width="30"></td>
                            <td width="580" valign="top" bgcolor="#FFFFFF"
                                style="border-collapse: collapse;font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#999;">
                                <table width="580" cellpadding="0" cellspacing="0" border="0"
                                       bgcolor="#FFFFFF">
                                    <tbody>
                                    <tr style="border-collapse: collapse;">
                                        <td width="580" height="18" valign="top" bgcolor="#FFFFFF"
                                            style="border-collapse: collapse;font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#999;">
                                            Airport Parking & Hotels Ltd - All rights reserved.
                                        </td>
                                    </tr>
                                    <tr style="border-collapse: collapse;">
                                        <td width="580" valign="top" height="18" bgcolor="#FFFFFF"
                                            style="border-collapse: collapse;font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#999;">
                                            Registered Office:Brockbourne House, 77 Mount Ephraim, Tunbridge Wells, Kent TN4 8BS. Registered in England. Company Registration Number: 1539777 VAT Number: GB 350 9836 37 <br />&nbsp;<br />
                                        </td>
                                    </tr>
                                    <tr style="border-collapse: collapse;">
                                        <td width="580" valign="top" bgcolor="#FFFFFF"
                                            style="border-collapse: collapse;font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#999;">
                                            The information in this e-mail is confidential and may be
                                            legally privileged. It is intended solely for the addressee.
                                            Access to this e-mail by anyone else is unauthorised. If you
                                            are not the intended recipient, any disclosure, copying,
                                            distribution or any action taken or omitted to be taken in
                                            reliance on it is prohibited and may be unlawful. If you
                                            receive this email unintentionally please delete it and
                                            contact the sender immediately.
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td width="30"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr style="border-collapse: collapse;">
                <td width="640" height="16"></td>
            </tr>
        </table>
        <table width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE">
            <tbody>
            <tr style="border-collapse: collapse;">
                <table width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE">
                    <tbody>
                    <tr style="border-collapse: collapse;">
                        <td width="554" height="41" valign="top"><img alt="www.aph.com" border="0"
                                                                      style="border:none; padding:0px; margin:0px; display:block; vertical-align:top;"
                                                                      src="http://aph.com/assets/images/email/footer.png"
                                                                      width="554" height="41"/></td>
                        <td valign="top" width="29" height="41"><a
                                    style="display:block; width:29px; height:41px; border:none; border-color:#fff; padding:0px; margin:0px;"
                                    href="https://www.facebook.com/AirportParkingHotels"
                                    target="_blank"><img alt="Facebook" border="0"
                                                         style="border:none; padding:0px; margin:0px; display:block; vertical-align:top;"
                                                         src="http://aph.com/assets/images/email/footer_facebook.gif"
                                                         width="29" height="41"/></a></td>
                        <td width="57" height="41" valign="top"><a
                                    style="display:block; width:29px; height:41px; border:none; border-color:#fff; padding:0px; margin:0px;"
                                    href="https://twitter.com/aphparking" target="_blank"><img
                                        alt="Twitter" border="0"
                                        style="border:none; padding:0px; margin:0px; display:block; vertical-align:top;"
                                        src="http://aph.com/assets/images/email/footer_twitter.gif"
                                        width="57" height="41"/></a></td>
                    </tr>
                    </tbody>
                </table>
            </tr>
            <tr style="border-collapse: collapse;">
                <td width="640" height="58" valign="top" colspan="3"><img border="0"
                                                                          style="border:none; padding:0px; margin:0px; display:block; vertical-align:top;"
                                                                          src="http://aph.com/assets/images/email/sub_footer.png"
                                                                          width="640" height="58"/></td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>