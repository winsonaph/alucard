{strip}<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <title>APH Portal - Flightboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN CSS TEMPLATE -->
    <link href="/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/css/flightboard.css" rel="stylesheet" type="text/css"/>
    <!-- END CSS TEMPLATE -->
</head>
<!-- BEGIN BODY -->
<body>
<!-- BEGIN PAGE CONTAINER-->
<div class="row">
    <div class="col-md-12">
        <img id="error_logo" src="/assets/uploads/error/APH-Logo.png">
    </div>
</div>
<!-- END PAGE -->
<!-- END CONTAINER -->
<script src="/assets/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="/assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets/js/error.js" type="text/javascript"></script>
</body>

</html>
{/strip}