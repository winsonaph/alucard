<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>APH Error Notification</title>

    <!-- include header styles -->
    {include file="../email-includes/header-styles.tpl"}
    <!-- // include header styles -->

</head>
<body style="background-color:#ebebeb; margin:0px; text-align:center;">
<table width="100%" cellpadding="0" cellspacing="0" border="0" id="background-table" bgcolor="#eeeeee"
       style="background-color: #eeeeee;">
    <tbody>
    <tr style="border-collapse: collapse;">
        <td align="center" bgcolor="#eeeeee" style="border-collapse: collapse;">
            <table style="margin: 0 10px;" width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
                <tbody>
                <tr style="border-collapse: collapse;">
                    <td height="30" align="left" valign="middle" bgcolor="#eeeeee"
                        style="border-collapse: collapse; background-color: #eeeeee;"></td>
                </tr>
                <tr style="border-collapse: collapse;">
                    <td align="left" valign="top" style="border-collapse: collapse;"><img
                                src="http://aph.com/assets/images/email/header.png" width="640" height="115" alt="APH"
                                title="" style="display:block; vertical-align:top; border:none;" border="0"/></td>
                </tr>
                <tr style="border-collapse: collapse;">
                    <td width="640" bgcolor="#FFFFFF"
                        style="border-collapse: collapse; font-family: Georgia, 'Times New Roman', Times, serif;">
                        <table width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
                            <tbody>
                            <tr style="border-collapse: collapse;">
                                <td width="30" height="30" bgcolor="#FFFFFF" style="border-collapse: collapse;"></td>
                                <td width="580" height="30" valign="top" bgcolor="#FFFFFF"
                                    style="border-collapse: collapse; font-family: Georgia, 'Times New Roman', Times, serif;"></td>
                                <td width="30" height="30" bgcolor="#FFFFFF" style="border-collapse: collapse;"></td>
                            </tr>
                            <tr style="border-collapse: collapse;">
                                <td width="30" height="30" bgcolor="#FFFFFF" style="border-collapse: collapse;"></td>
                                <td width="580" valign="top" bgcolor="#FFFFFF"
                                    style="border-collapse: collapse; font-family: Georgia, 'Times New Roman', Times, serif;">
                                    <table width="580" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
                                        <tbody>
                                        <tr style="border-collapse: collapse;">
                                            <td width="580" height="34" valign="top" bgcolor="#FFFFFF"
                                                style="border-collapse: collapse; font-family:Arial, Helvetica, sans-serif; font-size:24px; color:#666666;">
                                                Portal Error Notification
                                            </td>
                                        </tr>
                                        <tr style="border-collapse: collapse;">
                                            <td valign="top" bgcolor="#FFFFFF" height="43"
                                                style="border-collapse: collapse; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#666666;">
                                                You are receiving this email because an error occurred on the <strong>portal.aph.com</strong> application.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table width="580" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
                                        <tbody>
                                        <tr style="border-collapse: collapse;">
                                            <td width="580" valign="top" bgcolor="#FFFFFF"
                                                style="border-collapse: collapse;"><img
                                                        src="http://aph.com/assets/images/email/score-line.gif"
                                                        width="580" height="31"/></td>
                                        </tr>
                                        <tr style="border-collapse: collapse;">
                                            <td height="30" valign="top" bgcolor="#FFFFFF"
                                                style="border-collapse: collapse; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#666666; ">
                                                ERROR DETAILS
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table width="580" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
                                        <tbody>
                                        <tr style="border-collapse: collapse;">
                                            <td width="100" height="30" valign="top" bgcolor="#FFFFFF"
                                                style="border-collapse: collapse;"><img
                                                        src="http://aph.com/assets/images/email/ico-information.gif"
                                                        width="100" height="100"/></td>
                                            <td width="480" height="30" valign="top" bgcolor="#FFFFFF"
                                                style="border-collapse: collapse;">
                                                <table width="480" cellpadding="0" cellspacing="0" border="0"
                                                       bgcolor="#FFFFFF">
                                                    <tbody>
                                                    <tr>
                                                        <td width="480" height="30" valign="top" bgcolor="#FFFFFF"
                                                            style="border-collapse: collapse;font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#666666;">
                                                            <strong>Error Ref#:</strong><br />{$ERROR_REFERENCE_NUMBER}<br /><br />
                                                            <strong>Error message:</strong> <br />{$ERROR_MESSAGE}<br /><br />
                                                            <strong>Error file:</strong> <br />{$ERROR_FILE}<br /><br />
                                                            <strong>Error line:</strong> <br />{$ERROR_LINE}<br /><br />
                                                            <strong>Error trace:</strong> <br />{$ERROR_TRACE}<br /><br />
                                                            <strong>Error previous:</strong> <br />{$ERROR_PREVIOUS}<br /><br />
                                                            <strong>_REQUEST dump:</strong><br />{$REQUEST_DUMP|nl2br}<br /><br />
                                                            <strong>_SERVER dump:</strong><br />{$SERVER_DUMP|nl2br}<br /><br />
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="30" height="30" bgcolor="#FFFFFF" style="border-collapse: collapse;"></td>
                            </tr>
                            <tr style="border-collapse: collapse;">
                                <td width="30" height="30" bgcolor="#FFFFFF" style="border-collapse: collapse;"></td>
                                <td width="580" height="30" valign="top" bgcolor="#FFFFFF"
                                    style="border-collapse: collapse; font-family: Georgia, 'Times New Roman', Times, serif;"></td>
                                <td width="30" height="30" bgcolor="#FFFFFF" style="border-collapse: collapse;"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <!-- include footer -->
                {include file="../email-includes/footer.tpl"}
                <!-- // include footer -->
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
