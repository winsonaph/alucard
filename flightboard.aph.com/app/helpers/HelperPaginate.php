<?php
/**
 * Created by PhpStorm.
 * User: winsonlau
 * Date: 21/01/2015
 * Time: 10:16
 */

class HelperPaginate extends ControllerBase{

    public function paginate($data_list, $row_amount, $current_page, $before_after_button = 3, $left_over_button = 1){

        //Create a Model paginator, show 20 rows (default) by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $data_list,
                "limit"=> $row_amount,
                "page" => $current_page
            )
        );

        //Get the paginated results
        $page = $paginator->getPaginate();

        $this->view->PAGINATION_PAGE = $page;
        $this->view->PAGINATION_ITEMS_COUNT = COUNT($page->items);
        $this->view->BEFORE_AFTER_BUTTON = $before_after_button;
        $this->view->LEFT_OVER_BUTTON = $left_over_button;
    }
}
