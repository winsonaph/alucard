<?php

class IndexController extends ControllerBase
{
    public $maximum_flightboard = 5;
    public $maximum_row = 10;

    public function indexAction($carpark_label = null)
    {
        //Set where flightboard will get it images from.
        $this->view->IMG_SOURCE = "http://portal.aph.com/assets/uploads/flightscreens/original/";

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        /*$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);*/
        $this->view->pick('index');

        $router_param_list = $this->router->getParams();

        $date = date('Ymd');
        $time = date('His', strtotime('2 hour'));

        $carpark_list = PortalDB\CarParkLocation::query()
            ->where("name = :name:")
            ->bind(array("name" => $carpark_label))
            ->execute();

        $airport_id =  $carpark_list[0]->airport_id;
        $carpark_id =  $carpark_list[0]->id;

        try {

            $airport_data_list = PortalDB\Airports::query()
                ->where("id = :airport_id:")
                ->bind(array("airport_id" => $airport_id))
                ->execute();

            //If database can't find any data then only show promotion screens - otherwise will show white screens with error message.
            if (PortalDB\FlightData::query()
                    ->where("airport_id = :id: AND arrival_departure = :arr_dep: AND date >= :date: AND scheduled_time >= :time:")
                    ->bind(array("id" => $airport_data_list[0]->id, "arr_dep" => "D", "date" => $date, "time" => $time))
                    ->execute()->count() != 0
            ) {

                $flight_data_list = array();
                for ($x = 0; $x < $this->maximum_flightboard; $x++) {
                    $flight_data_list_result = PortalDB\FlightData::query()
                        ->where("airport_id = :id: AND arrival_departure = :arr_dep: AND date >= :date: AND scheduled_time >= :time:")
                        ->bind(array("id" => $airport_data_list[0]->id, "arr_dep" => "D", "date" => $date, "time" => $time))
                        ->limit($this->maximum_row, $x * $this->maximum_row)
                        ->execute();
                    if($flight_data_list_result->count()){
                        $flight_data_list[] = $flight_data_list_result;
                    }
                }
                $this->view->FLIGHT_DATA_LIST = $flight_data_list;

                //Will be used to show total flightboard number
                $this->view->MAXIMUM_FLIGHTBOARD = count($flight_data_list);
            }

        }
        catch(Exception $e){
            //** do not do anything funky with the error  */
        }



        $flight_screens_list = PortalDB\FlightScreens::query()
            ->where("carpark_id = :id: AND status = :status:")
            ->bind(array("id" => $carpark_id, "status" => 1))
            ->orderBy("id_order")
            ->execute();

        $this->view->FLIGHT_SCREENS_LIST = $flight_screens_list;
    }

    public function errorAction()
    {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->pick('error');
    }

}
