<?php

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

try {


    /**
     * Composer auto-loader
     */
    $config = include __DIR__ . "/../app/vendor/autoload.php";


    /**
     * Read the configuration
     */
    $config = include __DIR__ . "/../app/config/config.php";

    /**
     * Read auto-loader
     */
    include __DIR__ . "/../app/config/loader.php";

    /**
     * Read services
     */
    include __DIR__ . "/../app/config/services.php";

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

//    if($_SERVER['REMOTE_ADDR']=='192.168.201.89'){
//        throw new Exception('My message',1);
//    }

    echo $application->handle()->getContent();


} catch (\Exception $e) {
    $application = new \Phalcon\Mvc\Application($di);
    $dispatcher = $application->getDI()->get('dispatcher');
    $mailer = $application->getDI()->get('mailer');
    $smarty = $application->getDI()->get('smarty');


    //Handle other exceptions
    $dispatcher->forward(array(
        'controller' => 'index',
        'action' => 'error'
    ));

    $error_reference_number = time();

    /**
     * Prepare and send email with error details
     */
    $mailer->Subject = 'Portal error Ref#: '.$error_reference_number;
    $smarty->assign('ERROR_REFERENCE_NUMBER',$error_reference_number);
    $smarty->assign('ERROR_MESSAGE',$e->getMessage());
    $smarty->assign('ERROR_FILE',$e->getFile());
    $smarty->assign('ERROR_LINE',$e->getLine());
    $smarty->assign('ERROR_TRACE',$e->getTraceAsString());
    $smarty->assign('ERROR_PREVIOUS',$e->getPrevious());
    $smarty->assign('REQUEST_DUMP',print_r($_REQUEST,true));
    $server_dump = $_SERVER;

    /** remove cookie information */
    unset($server_dump['HTTP_COOKIE']);
    $smarty->assign('SERVER_DUMP',print_r($server_dump,true));
    $mailer->Body = $smarty->fetch('errors/email-generic-error.tpl');
    $mailer->send();

   $dispatcher->dispatch();


}
